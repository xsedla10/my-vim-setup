require "paq" {
  -- themes
  "wuelnerdotexe/vim-enfocado",
  "ayu-theme/ayu-vim",
  "mhartington/oceanic-next",

  -- nice features
  "nvim-tree/nvim-tree.lua",
  "nvim-tree/nvim-web-devicons",
  "nvim-lualine/lualine.nvim",
  "akinsho/toggleterm.nvim",
  "jiangmiao/auto-pairs",

  -- errors, lsp and completion
  "dense-analysis/ale",
  "neoclide/coc.nvim",
  "OmniSharp/omnisharp-vim"

}

require("nvim-tree").setup()
require("lualine").setup()
require("toggleterm").setup()

vim.cmd("let g:OmniSharp_server_use_net6 = 1")
vim.cmd("let g:OmniSharp_server_use_mono = 1")
