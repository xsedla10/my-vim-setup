vim.o.tabstop = 4
vim.o.shiftwidth = 4
vim.o.expandtab = true

vim.cmd.colorscheme('OceanicNext')
vim.wo.number = true
