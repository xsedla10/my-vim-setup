local applyTemplate = function (template, arguments)
  local builder = {}

  for i in 1 .. string.len(template) do
    for name, val in arguments do
      if string.sub(template, i, i + string.len(name) - 1) == name then
        table.insert(builder, val)
      end
    end
  end

  return table.concat(builder)
end

-- this function loads a template from a file; the allowed formats
-- are described ....
LoadTemplate = function (file_path)
end

-- the template must be create from a valid file via the LoadTemplate
-- function, the arguments must be a mapping from strings to strings,
-- the return value is a string
-- for example, if the arguments are "$data" and { data: "kitty" },
-- then the result will be "kitty"
-- $$ in a template is always converted to $, the keys of the arguments
-- must be ascii words using the 26 letters of the alphabet (upper
-- or lower case)
-- TODO: Add sections - ability to append multiple lines
ApplyTemplate = function (template, arguments)
  local skip = 0
  local builder = {}
  local expecting_arg = false

  for i = 1, #template do
    local ch = template:sub(i, i)

    if skip > 0 then
      skip = skip - 1
      goto continue
    end

    if not expecting_arg then
      if ch == '$' then
        expecting_arg = true
        goto continue
      end

      table.insert(builder, ch)
      goto continue
    end

    expecting_arg = false

    if ch == '$' then
      table.insert(builder, '$')
      goto continue
    end

    local inserted = false
    for name, value in pairs(arguments) do
      if string.sub(template, i, i + name:len() - 1) == name then
        table.insert(builder, value)
        skip = i + name:len() - 1
        inserted = true
        break
      end
    end

    if not inserted then
      table.insert(builder, "???")
    end

    ::continue::
  end

  return table.concat(builder)
end


-- test

print(ApplyTemplate("This is $$ and $what", { ["what"] = "i dont know" }))
