-- vim.api.nvim_set_option("clipboard", "unnamed")

-- vimwiki backup
vim.cmd([[autocmd BufWritePost *wiki !rclone copy ~/vimwiki/ "vimwiki dropbox backup":vimwiki]])

-- vimwiki calendar settings
vim.cmd([[
  let g:vimwiki_list = [{'path': '~/vimwiki/'}]
  let g:calendar_no_mappings=1
  set foldlevel=20
]])

vim.opt.foldmethod = 'indent'

vim.cmd([[au BufNewFile ~/vimwiki/diary/*.wiki :silent 0r !python ~/.local/share/nvim/generate_vimwiki_diary_template.py '%']])

--require("debugging")
