
week_template = """= {year} - Week {week_number} =

== Tasks ==


== Events ==
{events}


== Deadlines ==
{deadlines}


== This Week Happened ==


== This Week I Learned ==


== Thoughts =="""
