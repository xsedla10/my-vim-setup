from dataclasses import dataclass
from datetime import datetime
from enum import Enum
from itertools import chain


TAB_SIZE = 4


@dataclass(frozen=True, slots=True, order=True)
class Indentation:
    spaces: int

    @property
    def tabs(self) -> int:
        return TAB_SIZE * self.spaces

    def __str__(self) -> str:
        return ' ' * self.spaces

    def indent(self) -> 'Indentation':
        return Indentation(self.tabs + TAB_SIZE)

    @staticmethod
    def from_tabs(tabs: int) -> 'Indentation':
        return Indentation(tabs * TAB_SIZE)

    @staticmethod
    def from_spaces(spaces: int) -> 'Indentation':
        return Indentation(spaces)

    @staticmethod
    def get_line_indent(line: str) -> 'Indentation':
        spaces = 0

        for ch in line:
            match ch:
                case '\t':
                    spaces += TAB_SIZE

                case ' ':
                    spaces += 1

                case _:
                    return Indentation(spaces)

        return Indentation(0)

    @staticmethod
    def remove_line_indentation(line: str) -> str:
        for i, ch in enumerate(line):
            if ch != ' ' and ch != '\t':
                return line[i:]
        return ""


@dataclass(frozen=True, slots=True, order=True)
class VimwikiEvent:
    when: datetime
    description: str

    def to_export_str(self) -> str:
        when_str = self.when.strftime("%Y-%m-%d %H:%M")
        return f"- {when_str}\n    - {self.description}\n"

    @staticmethod
    def from_string(string: str) -> 'VimwikiEvent':
        lines = string.split("\n")

        when = datetime.strptime(lines[0][2:], "%Y-%m-%d %H:%M")
        description = lines[1][6:]

        return VimwikiEvent(when, description)


class VimwikiTaskStatus(Enum):
    INCOMPLETE = 0
    LT_ONE_THIRD = 1
    ONE_THIRD = 2
    TWO_THIRDS = 3
    COMPLETE = 4
    CANCELED = -1

    def __str__(self) -> str:
        match self:
            case VimwikiTaskStatus.INCOMPLETE:
                return ' '

            case VimwikiTaskStatus.LT_ONE_THIRD:
                return '.'

            case VimwikiTaskStatus.ONE_THIRD:
                return 'o'

            case VimwikiTaskStatus.TWO_THIRDS:
                return 'O'

            case VimwikiTaskStatus.COMPLETE:
                return 'X'

            case _:
                return '-'

    @staticmethod
    def from_string(string: str) -> 'VimwikiTaskStatus':
        match string:
            case ' ':
                return VimwikiTaskStatus.INCOMPLETE

            case '.':
                return VimwikiTaskStatus.LT_ONE_THIRD

            case 'o':
                return VimwikiTaskStatus.ONE_THIRD

            case 'O':
                return VimwikiTaskStatus.TWO_THIRDS

            case 'X':
                return VimwikiTaskStatus.COMPLETE

            case _:
                return VimwikiTaskStatus.CANCELED



@dataclass(frozen=True, slots=True, order=True)
class VimwikiTask:
    status: VimwikiTaskStatus
    description: str
    children: list['VimwikiTask']

    def export(self, indent: Indentation = Indentation(0)) -> str:
        string_builder = list[str]()
        string_builder.append(str(indent))
        string_builder.append(f"* [{str(self.status)}] ")
        string_builder.append(self.description)
        string_builder.append('\n')

        for child in self.children:
            child.export(indent.indent())

        return "".join(string_builder)

    @staticmethod
    def from_string(line: str) -> 'VimwikiTask':
        without_indent = Indentation.remove_line_indentation(line)

        status = VimwikiTaskStatus.from_string(without_indent[3])
        description = without_indent[5]

        return VimwikiTask(status, description, list())


@dataclass(frozen=True, slots=True)
class VimwikiEventDocument:
    upcoming: list[VimwikiEvent]
    passed: list[VimwikiEvent]

    def updated(self, current_time: datetime) -> 'VimwikiEventDocument':
        new_upcoming = list(filter(lambda ve: ve.when >= current_time,
                                   chain(self.upcoming, self.passed)))
        new_passed = list(filter(lambda ve: ve.when < current_time,
                                 chain(self.upcoming, self.passed)))

        return VimwikiEventDocument(new_upcoming, new_passed)

    def export(self, path: str) -> None:
        with open(path, "w") as f:
            f.write("= Events =\n\n")

            f.write("== Upcoming ==\n\n")
            for event in self.upcoming:
                f.write(event.to_export_str())
                f.write("\n")

            f.write("== Passed ==\n\n")
            for event in self.passed:
                f.write(event.to_export_str())
                f.write("\n")

    @staticmethod
    def import_from_file(path: str) -> 'VimwikiEventDocument':
        with open(path, "r") as f:
            for _ in range(4):
                f.readline()

            text = f.read()

        upcoming_strs, passed_strs = text.split("\n== Passed ==\n\n")
        upcoming_strs = filter(lambda s: not s.isspace(),
                               upcoming_strs.split('\n\n'))
        passed_strs = filter(lambda s: not s.isspace(),
                             passed_strs.split('\n\n'))

        upcoming = list(map(VimwikiEvent.from_string, upcoming_strs))
        passed = list(map(VimwikiEvent.from_string, passed_strs))

        return VimwikiEventDocument(upcoming, passed)


@dataclass(frozen=True, slots=True)
class VimwikiDeadlineDocument:
    upcoming: list[VimwikiEvent]
    passed: list[VimwikiEvent]

    def updated(self, current_time: datetime) -> 'VimwikiEventDocument':
        new_upcoming = list(filter(lambda ve: ve.when >= current_time,
                                   chain(self.upcoming, self.passed)))
        new_passed = list(filter(lambda ve: ve.when < current_time,
                                 chain(self.upcoming, self.passed)))

        return VimwikiEventDocument(new_upcoming, new_passed)

    def export(self, path: str) -> None:
        with open(path, "w") as f:
            f.write("= Deadlines =\n\n")

            f.write("== Upcoming ==\n\n")
            for event in self.upcoming:
                f.write(event.to_export_str())
                f.write("\n")

            f.write("== Passed ==\n\n")
            for event in self.passed:
                f.write(event.to_export_str())
                f.write("\n")

    @staticmethod
    def import_from_file(path: str) -> 'VimwikiEventDocument':
        with open(path, "r") as f:
            for _ in range(4):
                f.readline()

            text = f.read()

        upcoming_strs, passed_strs = text.split("\n== Passed ==\n\n")
        upcoming_strs = filter(lambda s: not s.isspace(),
                               upcoming_strs.split('\n\n'))
        passed_strs = filter(lambda s: not s.isspace(),
                             passed_strs.split('\n\n'))

        upcoming = list(map(VimwikiEvent.from_string, upcoming_strs))
        passed = list(map(VimwikiEvent.from_string, passed_strs))

        return VimwikiEventDocument(upcoming, passed)


@dataclass(frozen=True, slots=True)
class VimwikiWeekDocument:
    pass

# TODO: create the dask document, and the deadline document
# TODO: communication, automation
