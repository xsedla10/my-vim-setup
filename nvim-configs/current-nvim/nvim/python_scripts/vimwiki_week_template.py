from datetime import date, timedelta


def find_monday_in_second_week_of_year(year: int) -> date:
    year_start = date(year, 1, 1)

    day_change = 7 - year_start.weekday()

    return year_start + timedelta(days=day_change)


def count_week_number(current_date: date) -> int:
    num: int = 1

    week_start = find_monday_in_second_week_of_year(current_date.year)

    while week_start <= current_date:
        num += 1
        week_start += timedelta(days=1)

    return num


def is_in_week(some_date: date, week_start: date) -> bool:
    dif = some_date - week_start
    return dif.days >= 0 and dif.days <= 6



"""What i want to do?

I want to be able to load events from an event file

I want to be able to store them in the individual weeks

I should probably write a parser, which will turn it
into a class, with some clear methods. This way, I will
be able to get information from files somewhat easily.

This is however a much bigger thing; it will probably
take me a few hours to write...
"""
