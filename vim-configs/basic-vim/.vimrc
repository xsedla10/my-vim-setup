syntax on
set mouse=a
set smarttab
set number
set expandtab
set tabstop=4
set shiftwidth=4
set autoindent
set timeout timeoutlen=250
inoremap jj <ESC>
